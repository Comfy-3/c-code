#include <iostream>
#include <cmath>
using namespace std;

int main(){
	int x; //degrees
	float r; //radians	floats or doubles are real numbers
	float y;
	
	for(x=32;x<=126;x=x+15){
		r=float(x*(3.141592/180));
		y=sin(float(r));
		cout<<x<< " | "<<y<< "\n";
	}
	return 0;
}
