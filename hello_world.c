//Compile: gcc hello_world.c -o hello_world.o
#include <stdio.h>

int main(){
	printf("Hello World\n"); //English
	printf("Hola Mundo\n"); //Spanish
	printf("Hallo Welt\n"); //German
	printf("Haigh Ansin\n"); //Irish Gaelic
	printf("Salut Tout Le Monde\n"); //French
	printf("Shlom Lech Oalm\n"); //Hebrew
	printf("Hallo verden\n"); //Norwegian
	printf("Hej Verden\n"); //Danish
	printf ("Hallo Mensen\n"); //Dutch
	return 0;
}
