// helloworld.cpp compile: g++ helloworld.cpp -o helloworld.o
#include <iostream>

int main(){
	std::cout<<"Hello World \n"; //English
	std::cout<<"Hola Mundo \n"; //Spanish
	std::cout<<"Hallo Welt \n"; //German
	std::cout<<"Haigh Ansin \n"; //Irish Gaelic
	std::cout<<"Salut Tout Le Monde \n"; //French
	std::cout<<"Shlom Lech Oalm \n"; //Hebrew
	std::cout<<"Hallo verden \n"; //Norwegian
	std::cout<<"Hej Verden \n"; //Danish
	std::cout<<"Hallo Mensen \n"; //Dutch
}
